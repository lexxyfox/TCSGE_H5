# Time Clickers Save Game Editor for HTML5

A simple Time Clickers save game decoder / editor HTML5 application. Also an example Brunch project using Jade, Stylus, and Livescript.

Click to view:
[The online app](https://lexxyfox.gitlab.io/TCSGE_H5)
or the
[encryption algorithm](https://gitlab.com/lexxyfox/tcsge_h5/wikis/home)

**Note:** This project is only for educational purposes and/or for having fun. Please do not use for cheating!!! Thanks :)

## How to Fork
* Get started
  * Install [Node.js](http://nodejs.org) and NPM
  * Install [Brunch](http://brunch.io): `npm install -g brunch`
  * Clone the repo: `git clone https://gitlab.com/lexxyfox/tcsge_h5.git`
  * cd into repo
  * Install dependencies: `npm install`
* Make changes
* Compile: `brunch b`
* Have fun?!
